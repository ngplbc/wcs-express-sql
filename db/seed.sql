USE company;

INSERT INTO book (title, pub_date, available, pages) VALUES 
  ("Test title", "2010-02-12", 1, 321),
  ("Tools of Titans", "2016-02-12", 0, 301),
  ("The 4-Hour Workweek", "2010-02-19", 1, 261);
