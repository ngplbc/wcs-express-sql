const express = require("express");
const morgan = require("morgan");
const errorHandler = require("errorhandler");

const app = express();
const PORT = 3000;

const router = express.Router();

// middleware
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use("/", router);

// db
const con = require("./db/conf");

// get all and by column
const validateColumn = (req, res, next) => {
  const column = req.query.column;
  const columnsArr = ["id", "title", "pub_date", "available", "pages"];
  if (column && columnsArr.includes(column)) {
    req.column = column;
    next();
  }
  else if (!column) next();
  else res.sendStatus(400);
};

const validateFilter = (req, res, next) => {
  const filtersArr = ["contains", "starts", "greater"];
  if (req.query.str && filtersArr.includes(req.query.filter)) {
    req.filter = req.query.filter;
    req.str = req.query.str;
    next();
  } else if (!req.query.str && !req.query.filter) {
    next();
  } else res.sendStatus(400);
};

router.get("/books", validateColumn, validateFilter, (req, res, next) => {
  // filters
  if (req.filter && req.str) {
    let finalStr;
    let sql = `SELECT * FROM book WHERE ?? LIKE ?`;
    if (req.filter === "contains") {
      finalStr = "%" + req.str + "%";
    } else if (req.filter === "starts") {
      finalStr = req.str + "%";
    // TODO: id and pages can be compared to date type strings
    } else if (req.column === "id" || req.column === "pub_date" || req.column === "pages") {
      sql = `SELECT * FROM book WHERE ?? > ?`
      finalStr = req.str;
    } else res.sendStatus(400);
    con.query(sql, [req.column, finalStr], (err, rows) => {
      if (err) next(err);
      else res.status(200).json({ books: rows });
    })
  // no filters, all columns or specific ones
  } else {
    let column;
    req.column ? column = req.column : column = "*";
    const sql = `SELECT ?? FROM book;`;
    con.query(sql, [column], (err, rows) => {
      if (err) next(err);
      else res.status(200).json({ books: rows });
    })
  }
});

// get all sorted
const validateOrderBy = (req, res, next) => {
  if (req.params.by === "asc" || req.params.by === "desc") next();
  else res.sendStatus(400); 
};

router.get("/books/:by", validateColumn, validateOrderBy, (req, res, next) => {
  let sql;
  req.params.by === "asc" ? sql = `SELECT * FROM book ORDER BY ?? ASC;` : sql = `SELECT * FROM book ORDER BY ?? DESC;`;
  con.query(sql, [req.column, req.params.by], (err, rows) => {
    if (err) next(err);
    else res.status(200).json({ books: rows });
  })
});

// post book
router.post("/books", (req, res, next) => {
  const { book } = req.body;
  const sql = `INSERT INTO book SET ?;`;
  con.query(sql, book, (err, row) => {
    if (err) next(err);
    else {
      const sqlQuery = `SELECT * FROM book WHERE id = ${row.insertId};`
      con.query(sqlQuery, (err, row) => {
        if (err) next(err);
        else res.status(201).send(row)
      })
    };
  })
});

// modify book
router.put("/books/:id", (req, res, next) => {
  const id = req.params.id;
  const { book } = req.body;
  const sql = `UPDATE book SET ? WHERE id = ?;`;
  con.query(sql, [book, id], (err) => {
    if (err) next(err);
    else {
      const sqlQuery = `SELECT * FROM book WHERE id = ?;`
      con.query(sqlQuery, id, (err, row) => {
        if (err) next(err);
        else res.status(201).send(row)
      })
    }
  })
});

// toggle availability
router.put("/books/:id/availability", (req, res, next) => {
  const id = req.params.id;
  const sql = `SELECT available FROM book WHERE id = ?;`;
  con.query(sql, id, (err, row) => {
    if (err) next(err);
    else {
      const isAvailable = row[0].available;
      const sqlQuery = `UPDATE book SET ? WHERE id = ?;`
      con.query(sqlQuery, [isAvailable === 0 ? { available: 1 } : { available: 0 }, id], 
        (err) => {
          if (err) next(err);
          else {
            const lastSqlQuery = `SELECT * FROM book WHERE id = ?;`;
            con.query(lastSqlQuery, id, (err, row) => {
              if (err) next(err);
              else res.status(200).send(row);
            })
          }
        }
      );
    }
  });
});

// delete 
router.delete("/books/:id", (req, res, next) => {
  const id = req.params.id;
  const sql = `DELETE FROM book WHERE id = ?;`;
  con.query(sql, id, (err) => {
    if (err) next(err);
    else res.status(200).send({ message: `Item ${id} successfully deleted`});
  });
})

router.delete("/books", (req, res, next) => {
  const id = req.params.id;
  const sql = `DELETE FROM book WHERE available = 0;`;
  con.query(sql, id, (err) => {
    if (err) next(err);
    else res.sendStatus(200);
  });
})


app.use(errorHandler());

app.listen(PORT, () => console.log(`Server listening on port ${PORT}`));

